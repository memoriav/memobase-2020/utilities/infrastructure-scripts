### Helm Chart Cleanup

Running a job creates Kubernetes resources which need to be cleared from time to time. There is no 
automatic process to clear these.

The script 