import json
import subprocess

if __name__ == '__main__':

    with open('current_releases.json', 'r') as fp:
        releases = json.load(fp)

    for release in releases:
        if release['chart'].startswith('text-file-validation'):
            subprocess.run(["helm", "uninstall", release['name']])